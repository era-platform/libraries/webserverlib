.PHONY: all compile clean cleanall distclean

all: compile

compile:
	rebar3 compile

clean:
	rebar3 clean
	#rm -rf _build/default/lib/*/ebin/*.beam
	$(MAKE) dialclean

cleanall:
	rebar3 clean --all

distclean:
	rm -rf _build
	$(MAKE) dialclean

.PHONY: plt dial dialclean

plt:
	#dialyzer --build_plt --output_plt .dialyzer_plt -o .dialyzer_plt.out.txt --apps erts kernel stdlib -c deps/*/ebin
	dialyzer --build_plt --output_plt .dialyzer_plt -o .dialyzer_plt.out.txt --apps erts kernel stdlib

.dialyzer_plt: $(wildcard deps/ebin/*)
	$(MAKE) plt

dial: .dialyzer_plt
	#dialyzer --plt .dialyzer_plt -o dialyzer.out.txt ebin || (cat dialyzer.out.txt; exit 1)
	dialyzer --plt .dialyzer_plt -o dialyzer.out.txt deps/*/ebin ebin || (cat dialyzer.out.txt; exit 1)

dialclean:
	rm -f .dialyzer_plt .dialyzer_plt.out.txt dialyzer.out.txt
