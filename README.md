# Web server library for erlang applications.

Webserverlib is a library to create an embedded web server in erlang applications.
It is based on cowboy and provides convenience features on top of it.

## Installation

If you're using `rebar` in your project, just add the following to `rebar.config`:

```erlang
{deps, [
        {webserverlib, "https://gitlab.com/era-platform/libraries/webserverlib.git", "v1.0.0"}
       ]}
```

## Usage

Having `webserverlib` available your application needs to:
- implement http request handlers (using predefined behaviors)
- provide/register URL routes to dispatch to those handlers
- start web server listener on port

Normally the listener is started once on application start, and only has to be restarted if you want to start it on a different port.
Routes however can be updated at runtime: register new URL routes and unregister existing ones.

## Examples

### Implement http request handler

Basically the URL route handler module is completely responsible for handling incoming request.
It provides freedom to implement any behavior when necessary.
On the other hand, it is very common to reuse typical behaviors that can be factored out into separate modules.
Webserverlib provides a set of pre-defined behaviors, which you can readily use in your handlers.
For instance, there are REST collection and item handlers, file upload/download handlers, etc.
Every behavior requires its own set of callbacks implemented in your module.
Users are encouraged to write their own behaviors, possibly on top of provided ones.

Let's see an example of a REST endpoint handler:

```erlang
-module(myapp_ws_endpoint_handler).

-behavior(webserverlib_handler_behavior_endpoint).

-export([allowed_methods/2,
         get/2]).

allowed_methods(Req, State) -> [<<"GET">>].

get(Req, State) ->
    Content = <<"<html>\nHappy content">>,
    {ok, Content, Req, State}.
```

### Provide or register URL routes

It is possible to provide URL routes when starting listener on port. They will be configured immediately on start.
It is also possible to later register routes after listener is already running.
In both cases URL routes look like this:

```erlang
Routes = [{"/endpoint", myapp_ws_endpoint_handler, []}].
```

There are built-in handlers to return redirects and to serve static files from a folder.

```erlang
Routes = [{"/static", webserverlib_handler_redirect, [{to,"/static/"}]},
          {"/static/[...]", webserverlib_handler_static_folder, [{fs_root,"/var/www/html"},
                                                                 {auto_index,true},
                                                                 {auto_index_template,filename:join(code:priv_dir(myapp),"auto_index.html")}]}].
```

NB: The last example is not functioning yet, stay tuned..

### Configure web server to start listener on port

```erlang
Env = [{listener_default, #{port => 80,
                            protocol => http,
                            address => "0.0.0.0",
                            routes => Routes}}],
application:set_env([{webserverlib,Env}]).
```
