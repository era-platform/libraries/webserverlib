%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Dummy application using era-platform/webserverlib header file.
%%% -------------------------------------------------------------------

%% ===================================================================
%% Modules
%% ===================================================================

-define(APP, webserverlib).
-define(HU, webserverlib_handler_utils).
-define(SUPV, webserverlib_supv).
-define(COWBU, webserverlib_cowboy_utils).

%% ===================================================================
%% External modules
%% ===================================================================

-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).

%% ===================================================================
%% Macros
%% ===================================================================

-define(OUT(Fmt, Args), io:format(Fmt++"\n", Args)).
-define(DEBUG(Fmt, Args), io:format(Fmt++"\n", Args)).
%-define(DEBUG(Fmt, Args), case application:get_env(?APP, debug) of true -> io:format(Fmt++"\n", Args); _ -> ok end).
-define(LOG(Fmt, Args), ?BLlog:write(Fmt++"\n", Args), io:format(Fmt++"\n", Args)).
