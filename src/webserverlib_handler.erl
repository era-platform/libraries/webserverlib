%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 19.02.2021
%%% @doc Web server library handler of http requests.
%%% -------------------------------------------------------------------

-module(webserverlib_handler).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(cowboy_handler).

-export([init/2]).

-include("webserverlib.hrl").

%% ===================================================================
%% Callbacks
%% ===================================================================

-spec init(Req, State) -> {ok | module(), Req, State} | {module(), Req, State, X}
                            when Req :: cowboy_req:req(),
                                 State :: any(),
                                 X :: any().
init(Req, State) ->
    Req1 = cowboy_req:reply(200,
                            #{<<"content-type">> => <<"text/html; charset=utf-8">>},
                            <<"<html><div align='center'>Yee-haa!">>,
                            Req),
    {ok, Req1, State}.

