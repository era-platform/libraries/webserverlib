%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Web server library.
%%%   Application could be setup by application:set_env:
%%%      listener_* - keys with any suffix, from none to many.
%%%          Only values that are maps are considered.
%%%          Map structure must be as follows:
%%%              protocol - http or https
%%%              port - 1 to 65535
%%%              routes - cowboy routes (inner (paths) part excluding host). No virtual hosts are implemented at this time.
%%%              (dispatch) - cowboy dispatch (complete (hosts) dispatch). Virtual hosts could be implemented in future if required.
%%%   When listeners are pre-configured on application start, no start status is available immediately.
%%%   Status of individual listeners can be checked with the following functions after webserverlib application is running:
%%%   TODO design these functions.
%%%   When listeners are started using explicit functions after webserverlib application is already started, the start status is returned as result.
%%% -------------------------------------------------------------------

-module(webserverlib).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(application).

-export([start/2, stop/1]).

%-export([start_listener/1,
%         status_listener/1,
%         stop_listener/1]).

-include("webserverlib.hrl").

%% ===================================================================
%% Definitions
%% ===================================================================

-define(DEFAULT_START_ARGS,
        #{port => 8080,
          protocol => http,
          routes => undefined}). % 'undefined' will be substituted with cowboy_router:compile(?DEFAULT_ROUTES)
-define(DEFAULT_ROUTES, [{'_', [{"/", webserverlib_handler, []}]}]).

%% ===================================================================
%% Callbacks
%% ===================================================================

%% -------------------------------------------------------------------
%% Start web server with specified StartArgs.
%% StartArgs can be a map with necessary options or a list of maps
%%  when multiple ports need to be open and served by this application.
%% Map structure is as follows:
%% #{port => inet_port(),
%%   protocol => 'http' | 'https',
%%   cert_der := cert:der(), % - is only used when protocol==https, and so are the options below.
%%   cert_pem_path := filename:name(),
%%   cert_key_path := filename:name(),
%%   cert_key_pass := string() | binary()
%%  }
%% If at least one port is started, webserverlib's supervisor is also started.
%% -------------------------------------------------------------------
-spec start(StartType :: application:start_type(), StartArgs :: term()) ->
    {ok, pid()} | {ok, pid(), State :: term()} | {error, Reason :: term()}.
%% -------------------------------------------------------------------
start(_StartType, _StartArgs) ->
    io:format("application:get_all_env(?APP=~p) => ~120tp~n", [?APP,application:get_all_env(?APP)]),
    add_deps_paths(),
    StartArgs = get_start_args(?APP),
    start(StartArgs).

%% @private
add_deps_paths() ->
    DN = fun(P) -> filename:dirname(P) end,
    AppsDir = DN(DN(DN(code:which(?MODULE)))),
    DepApps = [basiclib, cowboy],
    DepPaths = [filename:join([AppsDir, Dep, "ebin"]) || Dep <- DepApps],
    code:add_pathsa(DepPaths).

%% @private
get_start_args(App) ->
    AllEnv = ?BU:get_all_env(App),
    LP = "listener_",
    F = fun({Key,Val}) when is_map(Val) ->
                KeyStr = ?BU:to_list(Key),
                case string:find(KeyStr, LP) =:= KeyStr of
                    true -> {true, Val};
                    false -> false
                end;
           (_KV) ->
                false
        end,
    lists:filtermap(F, AllEnv).

%% @private
start([]) -> % TODO remove this function clause
    start(?DEFAULT_START_ARGS);
start(StartArgs) when is_list(StartArgs) ->
    F = fun(StartArg) when is_map(StartArg) ->
                start_port(StartArg);
           (_) -> {error, invalid_args}
        end,
    lists:map(F, StartArgs),
    % start supervisor anyway - listeners may be started later.
    ?SUPV:start_link(StartArgs);
start(_) ->
    {error, invalid_args}.

%% @private
start_port(StartArg) ->
    F = fun(ranch_opts, Acc) -> Acc#{ranch_opts => make_ranch_opts(StartArg)};
           (cowboy_opts, Acc) -> Acc#{cowboy_opts => make_cowboy_opts(StartArg)}
        end,
    StartOpts = lists:foldl(F, #{}, [ranch_opts, cowboy_opts]),
    case {maps:get(ranch_opts,StartOpts),
          maps:get(cowboy_opts,StartOpts)} of
        {{ok,RanchOpts}, {ok,CowboyOpts}} ->
            StartCowboyFun = fun(http) -> cowboy:start_clear(http, RanchOpts, CowboyOpts);
                                (https) -> cowboy:start_tls(https, RanchOpts, CowboyOpts)
                             end,
            Protocol = maps:get(protocol, StartArg),
            StartCowboyFun(Protocol)
    end.

%% -------------------------------------------------------------------
stop(_State) -> ok.

%% ====================================================================
%% Public API
%% ====================================================================

%% ====================================================================
%% Internal
%% ====================================================================

make_ranch_opts(StartArg) ->
    case maps:get(port, StartArg, undefined) of
        undefined -> {error, {invalid_args, "port is missing"}};
        Port when is_integer(Port) andalso Port >= 1 andalso Port =< 65535 -> {ok, [{port, Port}]};
        Port when is_integer(Port) -> {error, {invalid_args, "port must fall in range 1..65535"}};
        _ -> {error, {invalid_args, "port must be integer"}}
    end.

make_cowboy_opts(StartArg) ->
    Routes = case maps:get(routes, StartArg, undefined) of
                 undefined -> cowboy_router:compile(?DEFAULT_ROUTES);
                 D -> D
             end,
    Dispatch = cowboy_router:compile([{'_', Routes}]), % work for all hosts, no virtualhosts implemented.
    Opts = #{env => #{dispatch => Dispatch}},
    {ok, Opts}.

