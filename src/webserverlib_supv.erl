%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Dummy application using era-platform/webserverlib.
%%% -------------------------------------------------------------------

-module(webserverlib_supv).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(supervisor).

-export([init/1]).

-export([start_link/1]).

%% ===================================================================
%% Callbacks
%% ===================================================================

%% -------------------------------------------------------------------
-spec init(StartArgs :: term()) ->
    {ok, {SupFlags :: supervisor:sup_flags(), [ChildSpec :: supervisor:child_spec()]}} | ignore.
%% -------------------------------------------------------------------
init(_StartArgs) ->
    SupFlags = #{strategy => one_for_one,
                 intensity => 0,
                 period => 5},
    %ChildSpecs = [#{id => webserverlib_srv,
    %                start => {webserverlib_srv, start, StartArgs},
    %                restart => permanent,
    %                shutdown => 5000,
    %                type => worker,
    %                modules => [webserverlib_srv]
    %               }],
    ChildSpecs = [],
    {ok, {SupFlags, ChildSpecs}}.

%% ====================================================================
%% Public API
%% ====================================================================

%% -------------------------------------------------------------------
-spec start_link(StartArgs::term()) -> supervisor:startlink_ret().
%% -------------------------------------------------------------------
start_link(StartArgs) ->
    supervisor:start_link({local,?MODULE}, ?MODULE, StartArgs).

