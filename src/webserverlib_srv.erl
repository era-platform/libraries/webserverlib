%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Dummy application using era-platform/webserverlib.
%%% -------------------------------------------------------------------

-module(webserverlib_srv).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(gen_server).

-export([init/1, handle_call/3, handle_cast/2]).

%% ===================================================================
%% Callbacks
%% ===================================================================

init(StartArgs) ->
    State = #{start_args => StartArgs},
    {ok, State}.

handle_call({test}, _From, State) ->
    Res = {test_response},
    {reply, Res, State};
handle_call(_Request, _From, State) ->
    {noreply, State}.

handle_cast(_Request, State) ->
    {noreply, State}.

