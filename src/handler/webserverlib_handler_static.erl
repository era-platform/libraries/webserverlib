%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 25.05.2021
%%% @doc Web server library handler for static files (folder or single file).
%%% -------------------------------------------------------------------

-module(webserverlib_handler_static).
-author('Anton Makarov <anton@mastermak.ru>').

-export([init/2]).
-export([content_types_provided/2]).
-export([get_file/2]).
-export([generate_etag/2,
         last_modified/2]).

-export([get_default_index_filename/0]).

%% ===================================================================
%% Definitions
%% ===================================================================

-include("webserverlib.hrl").

-include_lib("kernel/include/file.hrl").

%% ===================================================================
%% Callbacks
%% ===================================================================

init(Req0, Opts) ->
    Req = ?HU:allow_origin(Req0),
    [StaticOpts] = ?BU:extract_required_props([static_opts], Opts),
    StaticIdx = case ?BU:extract_optional_props([static_idx], Opts) of
                    [undefined] -> get_default_index_filename();
                    [SI] -> SI
                end,
    F = fun({cowboy_rest, Req1, _}) ->
            case ?BU:extract_optional_props([<<"processing_404_request">>], Opts) of
                [undefined] ->
                    [Static404] = ?BU:extract_required_props([static404], Opts),
                    NewOpts = lists:keystore(static_opts, 1, Opts, {static_opts, Static404}),
                    init(Req, [{<<"processing_404_request">>,true} | NewOpts]);
                _ -> {ok, cowboy_req:reply(404, Req1), nullstate}
            end
        end,
    ?COWBU:cowboy_static_idx(Req, StaticOpts, StaticIdx, F).

content_types_provided(Req, State) ->
    cowboy_static:content_types_provided(Req, State).

get_file(Req, State) ->
    cowboy_static:get_file(Req, State).

%% Enable cache based on processing If-None-Match and If-Modified-Since headers (and generate ETag and Last-Modified headers in response).

generate_etag(Req, State) ->
    cowboy_static:generate_etag(Req, State).

last_modified(Req, State) ->
    cowboy_static:last_modified(Req, State).

%% ====================================================================
%% Public functions
%% ====================================================================

get_default_index_filename() -> <<"index.html">>.

