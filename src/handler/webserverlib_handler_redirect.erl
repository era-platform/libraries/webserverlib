%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 25.02.2021
%%% @doc Web server library HTTP redirect handler
%%% -------------------------------------------------------------------

-module(webserverlib_handler_redirect).

-export([init/2, terminate/3]).

-export([moved/3]).

-include("webserverlib.hrl").

%% ====================================================================
%% Callbacks
%% ====================================================================

init(Req0, Opts) ->
    Req = ?HU:allow_origin(Req0),
    Req1 = moved(Req, Opts, <<>>),
    {ok, Req1, nullstate}.

terminate(_Reason, _Req, _State) ->
    ok.

%% ====================================================================
%% Public API
%% ====================================================================

moved(Req, Opts, BodyFun) when is_function(BodyFun) ->
    moved(Req, Opts, BodyFun());
moved(Req, Opts, Body) when is_binary(Body) ->
    [Location] = ?BU:extract_required_props([location], Opts),
    Req1 = cowboy_req:set_resp_header(<<"server">>, <<"WebserverLib">>, Req),
    Req2 = cowboy_req:set_resp_header(<<"location">>, ?BU:to_binary(Location), Req1),
    Req3 =
        case Body of
            <<>> -> Req2;
            _ -> cowboy_req:set_resp_body(Body, Req2)
        end,
    [Code] = ?BU:extract_optional_default([code], Opts, [302]),
    cowboy_req:reply(Code, Req3).
