%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Web server library handler utils.
%%% -------------------------------------------------------------------

-module(webserverlib_handler_utils).
-author('Anton Makarov <anton@mastermak.ru>').

-export([allow_origin/1]).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------------------------------------
%% Set Access-Control-* response headers
%% -------------------------------------------------------------------
-spec allow_origin(Req) -> Req when Req :: cowboy_req:req().
%% -------------------------------------------------------------------
allow_origin(Req) ->
    Host = cowboy_req:header(<<"host">>, Req, <<>>),
    Origin = cowboy_req:header(<<"origin">>, Req, Host),
    case lists:last(binary:split(Origin, <<"://">>, [])) of
        Host -> Req;
        _ ->
            Req1 = case cowboy_req:header(<<"access-control-request-method">>, Req, undefined) of
                       undefined -> Req;
                       M -> cowboy_req:set_resp_header(<<"access-control-allow-methods">>, M, Req)
                   end,
            Req2 = case cowboy_req:header(<<"access-control-request-headers">>, Req1, undefined) of
                       undefined -> Req1;
                       H-> cowboy_req:set_resp_header(<<"access-control-allow-headers">>, H, Req1)
                   end,
            Req3 = cowboy_req:set_resp_header(<<"access-control-allow-origin">>, Origin, Req2),
            _Req4 = cowboy_req:set_resp_header(<<"access-control-allow-credentials">>, <<"true">>, Req3)
    end.
