%%% coding: utf-8
%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 25.05.2021
%%% @doc Utilities to work with cowboy and its modules.

-module(webserverlib_cowboy_utils).
-author('Anton Makarov <anton@mastermak.ru>').

-export([cowboy_static/2,
         cowboy_static/3,
         cowboy_static_idx/3,
         cowboy_static_idx/4]).

-export([ipaddr/2]).

-include("webserverlib.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public API
%% ====================================================================

%% ----------------------------------
%% Handle request with cowboy_static module
%% ----------------------------------
cowboy_static(Req, StaticOpts) ->
    F404 = fun({cowboy_rest, Req1, _}) ->
               {ok, cowboy_req:reply(404, Req1), nullstate}
           end,
    cowboy_static_404_idx(Req, StaticOpts, F404, undefined).

%% ----------------------------------
%% Handle request with cowboy_static module and delegate 404 processing
%% ----------------------------------
cowboy_static(Req, StaticOpts, F404) ->
    cowboy_static_404_idx(Req, StaticOpts, F404, F404).

%% ----------------------------------
%% Handle request with cowboy_static module, redirect to Path with slash for dirs and serve index file for dir.
%% ----------------------------------
cowboy_static_idx(Req, StaticOpts, IndexFile) ->
    F404 = fun({cowboy_rest, Req1, _}) ->
               {ok, cowboy_req:reply(404, Req1), nullstate}
           end,
    cowboy_static_404_idx(Req, StaticOpts, F404, IndexFile).

%% ----------------------------------
%% Handle request with cowboy_static module, redirect to Path with slash for dirs, serve index file for dir and delegate 404 processing.
%% ----------------------------------
cowboy_static_idx(Req, StaticOpts, IndexFile, F404) ->
    cowboy_static_404_idx(Req, StaticOpts, F404, IndexFile).

%% ----------------------------------
%% Return ip address as binary string (or empty string on error).
%% ----------------------------------
ipaddr(Req, ErrLogFun2) ->
    {Peer,_} = cowboy_req:peer(Req),
    case inet:ntoa(Peer) of
        {error,ErrorIp} -> apply_log_fun(ErrLogFun2, Peer, ErrorIp),
            <<>>; % {error,einval} as inet:ntoa/1 spec says.
        Ip -> ?BU:to_binary(Ip)
    end.

%% @private
apply_log_fun(ErrLogFun2, Peer, ErrorIp) ->
    case erlang:is_function(ErrLogFun2) of
        true ->
            case erlang:fun_info(ErrLogFun2, arity) of
                {arity,2} -> ErrLogFun2(Peer, ErrorIp);
                _ -> ok
            end;
        false -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------
%% handle request with cowboy_static module, delegate 404 processing, redirect to Path with slash for dirs and serve index file for dir.
%% ----------------------------------
-spec cowboy_static_404_idx(Req, StaticOpts, F404, IndexFile) -> Req
                                                                   when Req :: cowboy_req:req(),
                                                                        StaticOpts :: cowboy_static:opts(),
                                                                        F404 :: fun(({cowboy_rest, Req, error | cowboy_static:state()}) ->
                                                                                        {ok, Req, State::term()}),
                                                                        IndexFile :: string() | binary() | undefined.
cowboy_static_404_idx(Req, StaticOpts, F404, IndexFile) ->
    case cowboy_static:init(Req, StaticOpts) of
        {cowboy_rest, Req1, {_FilePath, {direct,#file_info{type=directory}}, _}}=CSRes ->
            Path = cowboy_req:path(Req1),
            case binary:at(Path, byte_size(Path)-1) of
                $/ ->
                    case IndexFile of
                        undefined -> F404(CSRes);
                        _ ->
                            IndexPath = <<Path/binary,(?BU:to_binary(IndexFile))/binary>>,
                            %Req2 = cowboy_req:set([{path, IndexPath}, {path_info, split_path(IndexPath)}], Req1),
                            Req2 = Req1#{path => IndexPath, path_info => split_path(IndexPath)},
                            cowboy_static(Req2, StaticOpts, F404)
                    end;
                _ ->
                    DirPath = <<Path/binary,"/">>,
                    Req2 = cowboy_req:set_resp_header(<<"location">>, DirPath, Req1),
                    {ok, cowboy_req:reply(301, Req2), nullstate}
            end;
        {cowboy_rest, _, {_FilePath, {direct,#file_info{}}, _}}=CSRes ->
            CSRes;
        {cowboy_rest, Req1, {_FilePath, {error,eaccess}, _}} ->
            {ok, cowboy_req:reply(403, Req1), nullstate};
        {cowboy_rest, _Req1, {_FilePath, {error,E}, _}}=CI when E==enoent; E==enotdir; E==eisdir; E==enotsup ->
            F404(CI);
        {cowboy_rest, Req1, {FilePath, {error,E}, _}} ->
            ?OUT("~tp: request for ~ts cowboy_static returned {error,~tp}", [?MODULE, FilePath, E]), % TODO ?LOG
            {ok, cowboy_req:reply(500, Req1), nullstate};
        {cowboy_rest, Req1, {FilePath,_,_}=State} ->
            ?OUT("~tp: request for ~ts cowboy_static returned State=~tp", [?MODULE, FilePath, State]), % TODO ?LOG
            {ok, cowboy_req:reply(500, Req1), nullstate};
        {cowboy_rest, Req1, error} ->
            ?OUT("~tp: request for ~ts cowboy_static returned error", [?MODULE, cowboy_req:path(Req)]), % TODO ?LOG
            {ok, cowboy_req:reply(500, Req1), nullstate}
    end.

%% @private
%% This is a copy of cowboy_router:split_path/1 that is not exported there.
%% Following RFC2396, this function may return path segments containing any
%% character, including <em>/</em> if, and only if, a <em>/</em> was escaped
%% and part of a path segment.
-spec split_path(binary()) -> cowboy_router:tokens() | badrequest.
split_path(<< $/, Path/bits >>) ->
    split_path(Path, []);
split_path(_) ->
    badrequest.

split_path(Path, Acc) ->
    try
        case binary:match(Path, <<"/">>) of
            nomatch when Path =:= <<>> ->
                remove_dot_segments(lists:reverse([cow_uri:urldecode(S) || S <- Acc]), []);
            nomatch ->
                remove_dot_segments(lists:reverse([cow_uri:urldecode(S) || S <- [Path|Acc]]), []);
            {Pos, _} ->
                << Segment:Pos/binary, _:8, Rest/bits >> = Path,
                split_path(Rest, [Segment|Acc])
        end
    catch error:_ ->
        badrequest
    end.

remove_dot_segments([], Acc) ->
    lists:reverse(Acc);
remove_dot_segments([<<".">>|Segments], Acc) ->
    remove_dot_segments(Segments, Acc);
remove_dot_segments([<<"..">>|Segments], Acc=[]) ->
    remove_dot_segments(Segments, Acc);
remove_dot_segments([<<"..">>|Segments], [_|Acc]) ->
    remove_dot_segments(Segments, Acc);
remove_dot_segments([S|Segments], Acc) ->
    remove_dot_segments(Segments, [S|Acc]).
